package messagebus.domain;

import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

public class Producer implements Runnable {

	private static final Logger LOG = Logger.getLogger(Producer.class);

	private Long id;

	private Queue<Message> messageBus;

	private Semaphore semaphore;

	public Producer(Long id, Queue<Message> messageBus, Semaphore semaphore) {
		this.id = id;
		this.messageBus = messageBus;
		this.semaphore = semaphore;
	}

	@Override
	public void run() {
		Random random = new Random();
		Topic[] topics = Topic.values();
		while (true) {
			Message message = new Message(topics[random.nextInt(3)],
					"producer " + id + ", message " + random.nextInt(100));
			try {
				// Thread.sleep(300);
				semaphore.acquire();
				messageBus.add(message);
			} catch (InterruptedException e) {
				LOG.error(e);
			}
		}
	}
}
