package messagebus.domain;

import java.util.Queue;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

public class Consumer implements Runnable {

	private static final Logger LOG = Logger.getLogger(Producer.class);

	private Long id;

	private Queue<Message> messageBus;

	private Semaphore semaphore;

	private Topic topic;

	public Consumer(Long id, Queue<Message> messageBus, Semaphore semaphore, Topic topic) {
		this.id = id;
		this.messageBus = messageBus;
		this.semaphore = semaphore;
		this.topic = topic;
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(300);
				Message message = messageBus.peek();
				if (message != null && message.getTopic().equals(topic)) {
					message = messageBus.poll();
					LOG.info("consumer " + id + " " + message);
					semaphore.release();
				}
			} catch (InterruptedException e) {
				LOG.error(e);
			}

		}
	}
}
