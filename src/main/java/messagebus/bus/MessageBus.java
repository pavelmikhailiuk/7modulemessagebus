package messagebus.bus;

public interface MessageBus {

	void start();

	void stop();
}
