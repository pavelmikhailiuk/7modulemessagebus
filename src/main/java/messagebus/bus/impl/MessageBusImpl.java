package messagebus.bus.impl;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

import messagebus.bus.MessageBus;
import messagebus.domain.Consumer;
import messagebus.domain.Message;
import messagebus.domain.Producer;
import messagebus.domain.Topic;

public class MessageBusImpl implements MessageBus {

	private ExecutorService executorService;

	private int producersCount;

	private int queueSize;

	public MessageBusImpl(int producersCount, int queueSize) {
		this.producersCount = producersCount;
		this.queueSize = queueSize;
	}

	@Override
	public void start() {
		Queue<Message> messageBus = new SynchronizedQueue<>(new ArrayDeque<>());
		Semaphore semaphore = new Semaphore(queueSize);
		Topic[] topics = Topic.values();
		executorService = Executors.newFixedThreadPool(producersCount + topics.length);
		for (int i = 0; i < producersCount; i++) {
			executorService.submit(new Producer(Long.valueOf(i), messageBus, semaphore));
		}
		for (int i = 0; i < topics.length; i++) {
			executorService.submit(new Consumer(Long.valueOf(i), messageBus, semaphore, topics[i]));
		}
		executorService.shutdown();
	}

	@Override
	public void stop() {
		if (executorService != null) {
			executorService.shutdownNow();
		}
	}
}
