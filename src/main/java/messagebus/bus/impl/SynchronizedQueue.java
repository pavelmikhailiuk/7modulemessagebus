package messagebus.bus.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class SynchronizedQueue<E> implements Queue<E> {

	private Queue<E> queue;

	public SynchronizedQueue(Queue<E> queue) {
		this.queue = queue;
	}

	@Override
	public synchronized boolean add(E e) {
		return queue.add(e);
	}

	@Override
	public synchronized boolean offer(E e) {
		return queue.offer(e);
	}

	@Override
	public synchronized int size() {
		return queue.size();
	}

	@Override
	public synchronized boolean isEmpty() {
		return queue.isEmpty();
	}

	@Override
	public synchronized boolean contains(Object o) {
		return queue.contains(o);
	}

	@Override
	public synchronized E remove() {
		return queue.remove();
	}

	@Override
	public synchronized E poll() {
		return queue.poll();
	}

	@Override
	public synchronized E element() {
		return queue.element();
	}

	@Override
	public synchronized Iterator<E> iterator() {
		return queue.iterator();
	}

	@Override
	public synchronized E peek() {
		return queue.peek();
	}

	@Override
	public synchronized Object[] toArray() {
		return queue.toArray();
	}

	@Override
	public synchronized <T> T[] toArray(T[] a) {
		return queue.toArray(a);
	}

	@Override
	public synchronized boolean remove(Object o) {
		return queue.remove(o);
	}

	@Override
	public synchronized boolean containsAll(Collection<?> c) {
		return queue.containsAll(c);
	}

	@Override
	public synchronized boolean addAll(Collection<? extends E> c) {
		return queue.addAll(c);
	}

	@Override
	public synchronized boolean removeAll(Collection<?> c) {
		return queue.removeAll(c);
	}

	@Override
	public synchronized boolean retainAll(Collection<?> c) {
		return queue.retainAll(c);
	}

	@Override
	public synchronized void clear() {
		queue.clear();
	}
}
