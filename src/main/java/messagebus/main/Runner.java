package messagebus.main;

import messagebus.bus.MessageBus;
import messagebus.bus.impl.MessageBusImpl;

public class Runner {
	public static void main(String[] args) {
		MessageBus bus = new MessageBusImpl(3, 5);
		bus.start();
	}
}
